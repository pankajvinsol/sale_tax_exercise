class Input

  AMOUNT_REGEX = /^([0-9]{1,9})$|^([0-9]{1,9})(\.[0-9]{1,2})?$/

  def self.get_option(input_message = '', *value_list)
    input = ''
    until value_list.include? input
      print input_message
      input = gets.chomp
    end
    input
  end

  def self.get_number(input_message = '')
    input = ''
    until input =~ AMOUNT_REGEX
      print input_message
      input = gets.chomp
    end
    input
  end

end

