require_relative 'product.rb'

class BilledItem < Product
  attr_reader :sale_tax, :import_tax, :total_cost
  def initialize(product, sale_tax, import_tax, total_cost)
    super(product.name, product.price, product.sale_tax_exempted, product.imported)
    @sale_tax = sale_tax
    @import_tax = import_tax
    @total_cost = total_cost
  end
end

