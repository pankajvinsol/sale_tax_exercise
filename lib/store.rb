require_relative 'cart.rb'
require_relative 'product.rb'
require_relative 'bill.rb'
require_relative 'billed_item.rb'
require_relative 'input.rb'

class Store

  N_REGEX = /^n$/i
  SALE_TAX = 0.10
  IMPORT_TAX = 0.05

  def self.take_order
    cart = Cart.new
    continue = ""
    while not continue =~ N_REGEX
      print "Name of the product: "
      name = gets.chomp.squeeze
      imported = Input.get_option('Imported? ', 'yes', 'no')
      exempted_from_sale_tax = Input.get_option('Exempted from sales tax? ', 'yes', 'no')
      price = Input.get_number('Price: ')
      continue = Input.get_option('Do you want to add more items to your list(y/n): ', 'y', 'n')
      cart.add_item(Product.new(name, price.to_f.round(2), exempted_from_sale_tax, imported))
    end
    cart
  end

  def self.checkout(cart)
    bill = Bill.new
    cart.item_list.each { |item| bill.add_item(calculate_tax(item)) }
    bill
  end

  def self.calculate_tax(item)
    sale_tax = item.sale_tax_exempted == 'yes' ? 0.00 : (SALE_TAX * item.price).round(2)
    import_tax = item.imported == 'yes' ? (IMPORT_TAX * item.price).round(2) : 0.00
    total_cost = (item.price + sale_tax + import_tax).round(2)
    BilledItem.new(item, sale_tax, import_tax, total_cost)
  end

  private_class_method :calculate_tax

end

