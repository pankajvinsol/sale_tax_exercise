class Bill

  DASH_LINE = '-' * 79 + "\n"
  SEPARATOR = '|'

  def initialize
    @billed_item_list = []
    @grand_total = 0
  end
  def add_item(billed_item)
    @billed_item_list.push(billed_item)
    @grand_total += billed_item.total_cost
  end
  def show
    output = header
    @billed_item_list.each_with_index do |billed_item, index|
      output += generate_row(billed_item, index + 1)
    end
    output += footer
  end

  def header
    DASH_LINE + 'S.No.' + SEPARATOR + 'Item Name           ' + SEPARATOR + 'Imported Tax' + SEPARATOR + 'Sale Tax    ' + SEPARATOR + 'Price       ' + SEPARATOR + 'Total Price ' + "\n" + DASH_LINE
  end
    
  def footer
    footer_text = DASH_LINE
    footer_text += 'Grand Total' + ' ' * 54 + SEPARATOR + format_amount(@grand_total.round, 12)  + "\n"
    footer_text += DASH_LINE
  end

  def generate_row(billed_item, index)
    row = ""
    row += format_amount(index, 5) + SEPARATOR
    row += format_string(billed_item.name, 20) + SEPARATOR
    row += format_amount(billed_item.import_tax, 12) + SEPARATOR
    row += format_amount(billed_item.sale_tax, 12) + SEPARATOR
    row += format_amount(billed_item.price, 12) + SEPARATOR
    row += format_amount(billed_item.total_cost, 12) + "\n"
  end
  def format_string(string, length)
    string.to_s.length > length ? (string.to_s[0..length-4] + '...') : string.to_s.ljust(length)
  end

  def format_amount(amount, length)
    amount.to_s.rjust(length)
  end
end

