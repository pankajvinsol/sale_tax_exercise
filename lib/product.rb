class Product
  attr_reader :name, :price, :sale_tax_exempted, :imported
  def initialize(name, price, sale_tax_exempted, imported)
    @name = name
    @price = price
    @sale_tax_exempted = sale_tax_exempted
    @imported = imported
  end
end

